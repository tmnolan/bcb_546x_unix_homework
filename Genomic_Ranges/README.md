### Trevor Nolan BCB 546X

# Genomic Ranges Assignment

* R Markdown notebook to document the Genomic Ranges example: `Genomic_Ranges.Rmd`.
	* The output file for Mouse chr1 SNPs with number of variants per exon appended: `chr1_collapsed_exons.csv`.

# Extra Credit

* R Markdown notebook for getting mouse promoter ranges and seqs: `Promoter_Ranges.Rmd`.
	* output file for promoter sequences: `mm10_chr1_3kb_promoters.fasta`.