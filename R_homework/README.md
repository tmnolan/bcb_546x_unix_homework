# R_homework

* Project layout according to `ProjectTemplate`

##Assignment R Notebook
* **`R_homework/R_Assignment_TN.Rmd`**: contains R notebook with assignment. 
	* The code is designed to be portable and can be run using the "knit" function so long as the `R_Assignment_TN.Rmd` file is inside the `R_homework/` directory. 

* Rendered Versions of the assignment
	* `R_homework/R_Assignment_TN.PDF`: A PDF of the of the notebook 
	* `R_homework/R_Assignment_TN.html`: HTML version of notebook

##Output files from Data Processing
* located in **`R_homework/data/`**
	* `R_homework/data/maize/asc/` contains the files for maize seperated by chromosome in ascending order of position with missing data encoded as "?"
	* `R_homework/data/maize/dsc/` contains the files for maize seperated by chromosome in descending order with missing data encoded as "-"
	* `data/teosinte/asc/` contains the files for teosinte seperated by chromosome in ascending order of position with missing data encoded as "?"
	* `data/teosinte/dsc/` contains the files for teosinte seperated by chromosome in descending order with missing data encoded as "-"