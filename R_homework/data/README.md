# Data files from R_homework assignment
* `R_homework/data/maize/asc/` contains the files for maize seperated by chromosome in ascending order of position with missing data encoded as "?"
* `R_homework/data/maize/dsc/` contains the files for maize seperated by chromosome in descending order with missing data encoded as "-"
* `data/teosinte/asc/` contains the files for teosinte seperated by chromosome in ascending order of position with missing data encoded as "?"
* `data/teosinte/dsc/` contains the files for teosinte seperated by chromosome in descending order with missing data encoded as "-"