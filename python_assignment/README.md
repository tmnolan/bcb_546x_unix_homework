# Python Assignment BCB546 - Trevor Nolan:

The workflow of this assignment can be found in the following Jupyter notebook : 

**`Python homework TN.ipynb`** - [notebook link](https://nbviewer.jupyter.org/urls/bitbucket.org/tmnolan/bcb_546x_unix_homework/raw/2e96eb43bed85832cf37dec63ac82fdd7c84185d/python_assignment/Python%20homework%20TN.ipynb)

assignment details and some of the functions used are contained in `python_translate.py`


**Input data analyzed:** 

* **`bears_cytb.fasta`**: FASTA seqs of Cytochrome-b for 9 bear species

* **`bears_data.csv`**: species names and mass values for the bear species


**Data generated**

* **`bears_ctyb_aa_anal.csv`**: bears_data.csv data with proportion of amino acids added for the following categories, each of which is indicated as a column in the data for each bear species. 

    charged = ['R','K','D','E']
    polar = ['Q','N','H','S','T','Y','C','M','W']
    hydrophobic = ['A','I','L','F','V','P','G']
    
* **`bears_translations.fasta`**: translation of all 9 bear cytochrome-b sequences from `bears_cytb.fasta`. - part of #9 - just for fun. 
    
    




