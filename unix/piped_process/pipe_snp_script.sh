#!/bin/bash
set -e
set -u
set -o pipefail
#script to process snp files for unix assignment

#keep heads when sorting out maize and teosinte genotypes using grep and then transpose them using transpose.awk
(head -n1 fang_et_al_genotypes.txt && grep -E -w "ZMMIL|ZMMLR|ZMMMR" fang_et_al_genotypes.txt)| \
awk -f transpose.awk | tail -n+4 | \
sort -k1,1 > transposed_maize_genotypes_sorted.txt

(head -n1 fang_et_al_genotypes.txt && grep -E -w "ZMPBA|ZMPIL|ZMPJA" fang_et_al_genotypes.txt) | \
awk -f transpose.awk | tail -n+4 | \
sort -k1,1 > transposed_teosinte_genotypes_sorted.txt

#get columns needed from snp position file and sort it to prepare for joining
cut -f1,3,4 snp_position.txt | \
tee snp_position_short.txt | \
tail -n+2 | sort -k1,1 > snp_position_short_sorted.txt

#join the datasets by SNP_ID column, sort them based on ascending (asc) or descending (dsc) order and replace missing values in dsc files with "-"
(head -n1 snp_position_short.txt && join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_teosinte_genotypes_sorted.txt) | \
awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' | \
tee total_teosinte_genotypes_asc.txt | sed 's/?/-/g' | \
awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_teosinte_genotypes_dash_dsc.txt

(head -n1 snp_position_short.txt && join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_maize_genotypes_sorted.txt) | \
awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' | \
tee total_maize_genotypes_asc.txt | sed 's/?/-/g' | \
awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_maize_genotypes_dash_dsc.txt

#check results using wc
wc total_*.txt snp_position_short*.txt > join_check.txt

#check one set of results using awk to count columns
awk -F "\t" 'NR>1{print NF; exit}' total_maize_genotypes_dash_dsc.txt > join_check_cols.txt
awk -F "\t" 'NR>1{print NF; exit}' transposed_maize_genotypes_sorted.txt >> join_check_cols.txt

#make directories to store output files
mkdir -p snps_by_chr/{maize_asc,maize_dsc,teosinte_asc,teosinte_dsc}/

#copy files with all chromosomes that need to be split
mv total_maize_genotypes_asc.txt total_maize_genotypes_dash_dsc.txt total_teosinte_genotypes_asc.txt total_teosinte_genotypes_dash_dsc.txt snps_by_chr/

#use awk to separate out based on chromosome and store the results within snps_by_chr in appropriate directories
cd snps_by_chr/
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_asc.txt > maize_asc/snps_chr$chr.txt; done
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_dash_dsc.txt > maize_dsc/snps_chr$chr.txt; done
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_asc.txt > teosinte_asc/snps_chr$chr.txt; done
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_dash_dsc.txt > teosinte_dsc/snps_chr$chr.txt; done