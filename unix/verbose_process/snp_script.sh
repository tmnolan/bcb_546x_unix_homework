#!/bin/bash
set -e
set -u
set -o pipefail

#script to process snp files for unix assignment
#keep heads when sorting out maize and teosinte genotypes
head -n1 fang_et_al_genotypes.txt > maize_genotypes.txt
head -n1 fang_et_al_genotypes.txt > teosinte_genotypes.txt
#use grep to separate the maize and teosinte snps
grep -E -w "ZMMIL|ZMMLR|ZMMMR" fang_et_al_genotypes.txt >> maize_genotypes.txt #for maize genotypes
grep -E -w "ZMPBA|ZMPIL|ZMPJA" fang_et_al_genotypes.txt >> teosinte_genotypes.txt # for teosinte genotypes
#transpose the genotype files
awk -f transpose.awk maize_genotypes.txt > transposed_maize_genotypes.txt
awk -f transpose.awk teosinte_genotypes.txt > transposed_teosinte_genotypes.txt
#get columns needed from snp position file
cut -f1,3,4 snp_position.txt > snp_position_short.txt

# get rid of header lines
tail -n+4 transposed_maize_genotypes.txt | sort -k1,1 > transposed_maize_genotypes_sorted.txt 
tail -n+4 transposed_teosinte_genotypes.txt | sort -k1,1 > transposed_teosinte_genotypes_sorted.txt 
tail -n+2 snp_position_short.txt | sort -k1,1 > snp_position_short_sorted.txt

#join the datasets by SNP_ID column
join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_teosinte_genotypes_sorted.txt > joined_teosinte_genotypes.txt
join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_maize_genotypes_sorted.txt > joined_maize_genotypes.txt

#check join using wc
wc joined_* snp_position_short_sorted.txt > join_check.txt

#add SNP_ID Chromosome and Position headers back
head -n1 snp_position_short.txt > total_teosinte_genotypes.txt
head -n1 snp_position_short.txt > total_maize_genotypes.txt
cat joined_teosinte_genotypes.txt >> total_teosinte_genotypes.txt
cat joined_maize_genotypes.txt >> total_maize_genotypes.txt

#sort based on ascending order
cat total_teosinte_genotypes.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' > total_teosinte_genotypes_asc.txt
cat total_maize_genotypes.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' > total_maize_genotypes_asc.txt


#make dsc set with ? replaces with - 
sed 's/?/-/g' total_maize_genotypes.txt > total_maize_genotypes_dash.txt
sed 's/?/-/g' total_teosinte_genotypes.txt > total_teosinte_genotypes_dash.txt

#sort based on descending order
cat total_teosinte_genotypes_dash.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_teosinte_genotypes_dash_dsc.txt
cat total_maize_genotypes_dash.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_maize_genotypes_dash_dsc.txt
#make directories for the final output
mkdir -p snps_by_chr/{maize_asc,maize_dsc,teosinte_asc,teosinte_dsc}/

#copy files with all chromosomes together that need to be split
cp total_maize_genotypes_asc.txt total_maize_genotypes_dash_dsc.txt total_teosinte_genotypes_asc.txt total_teosinte_genotypes_dash_dsc.txt snps_by_chr/

#use awk to separate out based on chromosome
cd snps_by_chr/
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_asc.txt > maize_asc/snps_chr$chr.txt; done
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_dash_dsc.txt > maize_dsc/snps_chr$chr.txt; done
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_asc.txt > teosinte_asc/snps_chr$chr.txt; done
for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_dash_dsc.txt > teosinte_dsc/snps_chr$chr.txt; done
