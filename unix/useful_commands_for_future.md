##Data processing

* trying to separate `fang_et_al_genotypes.txt` into maize and teosinte genotypes

		head -n1 fang_et_al_genotypes.txt > maize_genotypes.txt
		head -n1 fang_et_al_genotypes.txt > teosinte_genotypes.txt
		
* then append each file with the genotypes matching the groups
	* for maize the Group = ZMMIL, ZMMLR, and ZMMMR 
	* for teosinte the Group = ZMPBA, ZMPIL, and ZMPJA
	
			grep -E -w "ZMMIL|ZMMLR|ZMMMR" fang_et_al_genotypes.txt >> maize_genotypes.txt #for maize genotypes
			grep -E -w "ZMPBA|ZMPIL|ZMPJA" fang_et_al_genotypes.txt >> teosinte_genotypes.txt # for teosinte genotypes		
			
* transpose the file for the maize or teosinte groups with the transpose.awk script
		
		awk -f transpose.awk maize_genotypes.txt > transposed_maize_genotypes.txt
		awk -f transpose.awk teosinte_genotypes.txt > transposed_teosinte_genotypes.txt
		
* next get only the columns needed from the `snp_position.txt` file

		cut -f1,3,4 snp_position.txt > snp_position_short.txt
		
* sort and join the `snp_position_short.txt` file with the `transposed_maize_genotypes.txt` and `transposed_teosinte_genotypes.txt` files using the SNP_ID (first) column.

		# get rid of header lines
		tail -n+4 transposed_maize_genotypes.txt | sort -k1,1 > transposed_maize_genotypes_sorted.txt 
		tail -n+4 transposed_teosinte_genotypes.txt | sort -k1,1 > transposed_teosinte_genotypes_sorted.txt 
		tail -n+2 snp_position_short.txt | sort -k1,1 > snp_position_short_sorted.txt
		
		#join the datasets by SNP_ID column
		join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_teosinte_genotypes_sorted.txt > joined_teosinte_genotypes.txt
		join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_maize_genotypes_sorted.txt > joined_maize_genotypes.txt
		
		#check join using wc
		wc joined_* snp_position_short_sorted.txt > join_check.txt
		
		#add headers back
		head -n1 snp_position_short.txt > total_teosinte_genotypes.txt
		head -n1 snp_position_short.txt > total_maize_genotypes.txt
		cat joined_teosinte_genotypes.txt >> total_teosinte_genotypes.txt
		cat joined_maize_genotypes.txt >> total_maize_genotypes.txt
		
		#make a set with ? replaces with - 
		sed 's/?/-/g' total_maize_genotypes.txt > total_maize_genotypes_dash.txt
		sed 's/?/-/g' total_teosinte_genotypes.txt > total_teosinte_genotypes_dash.txt
* make new directories for maize or teosinte data and move the files to them		
		
		mkdir maize_asc maize_dsc teosinte_asc teosinte_dsc
		cp total_maize_genotypes.txt maize/
		cp total_teosinte_genotypes.txt teosinte/
		
* break data out by chromosome - need to do this for each folder 

		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes.txt > snps_chr$chr.txt; done
		# this one works well - saved in script
		cat *.txt | for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' > snps_chr$chr.txt; done
		
		
		# another option that works but also makes files for unknown or multiple chromosomes
		awk 'NR==1{header=$0}NR>1&&!a[$2]++{print header > ("chr"$2".txt")}NR>1{print > ("chr"$2".txt")}' total_maize_genotypes.txt
		
		#try to do on all directories at once - not working
		
		for f in maize_*/ do (cd "$f"; awk 'NR==1{header=$0}NR>1&&!a[$2]++{print header > ("chr"$2".txt")}NR>1{print > ("chr"$2".txt")}' total_maize_genotypes_asc.txt) done
		
		#alternative
		for f in */ ; do awk 'FNR > 1' *.csv | sort -sk 1,2 | sort -sk 3,3 > ${f}_appended.dat; done
