#### This folder is related to assignments in BCB546X in Spring 2017.

# Genomic Ranges

The genomic ranges assignment and extra credit are located in `Genomic_Ranges/`



#Unix Assignment

##Data inspection
The following files were provided for the analysis:

1. `fang_et_al_genotypes.txt`: 
	* contains 2783 lines,  2744038 words and 11051939 characters.
	* has 986 columns (tab delimited spacing). Three of these (Sample_ID, JG_OTU, and Group) will need to be removed for the downstream analysis. I did this after separating the file by species and transposing the files. 
	* 11 Mb file size.
	* first row is a header containing SNP_IDs and other information. 
	* needs to be transposed using transpose.awk to be in the same format as the `snp_posittion.txt` file that is described below.
2. `snp_position.txt`
	* contains 984 lines, 13198 words, and 82763 characters. 
	* has 15 columns (tab delimited spacing). 
	* 1st, 3rd and 4th columns have the information that we are interested in (SNP_ID, Chromosome, Position).
	* First row is a header. 
	* 81 Kb file size.
3. `transpose.awk`: an awk script to transpose the fang_et_al_genotypes.txt file (outside scope of what we'd covered in class so far). 

##Data processing

###I wrote two methods to process the data files, which produce the same output, but differ in the intermediate files produced. 

1. **`piped_process/`: This directory contains the script `pipe_snp_script.sh`, which produces the 40 desired output files within `snps_by_chr/` (described further below). Each step of the process is documented in this script with appropriate comments.**

	* the content of the directory can be produced entirely by: 
		
			cd piped_process/	 #move to the piped_process folder
			./pipe_snp_script.sh #run the script on the folder already containing the input files.
			
	* the final output files are contained within `snps_by_chr/`. Each set of files is split out based on chromosome location of SNPs and named as follows: 

			snps_chr1.txt
			snps_chr2.txt
			snps_chr3.txt
			snps_chr4.txt
			snps_chr5.txt
			snps_chr6.txt
			snps_chr7.txt
			snps_chr8.txt
			snps_chr9.txt
			snps_chr10.txt
			
	* the output files are contained in the following directories within `snps_by_chr/`.
		* `maize_asc/` : SNPs from maize (Group = ZMMIL, ZMMLR, and ZMMMR) with SNPs ordered based on increasing position values and with missing data encoded by this symbol: ?

		* `maize_dsc/` : SNPs from maize (Group = ZMMIL, ZMMLR, and ZMMMR) with SNPs ordered based on decreasing position values and with missing data encoded by this symbol: -

		* `teosinte_asc/`: SNPs from teosinte (Group = ZMPBA, ZMPIL, and ZMPJA) with SNPs ordered based on increasing position values and with missing data encoded by this symbol: ?
		
		* `teosinte_dsc/`: SNPs from teosinte (Group = ZMPBA, ZMPIL, and ZMPJA) with SNPs ordered based on decreasing position values and with missing data encoded by this symbol: -

2. **`verbose_process/` : This directory contains the script `snp_script.sh`, a script that generates intermediate files for most steps and was helpful to me in writing the script from beginning to end and checking the results.**

###The scripts are included below so that my entire process can be viewed within the README file. 

* `pipe_snp_script.sh`: 

		#!/bin/bash
		set -e
		set -u
		set -o pipefail
		#script to process snp files for unix assignment
		
		#keep heads when sorting out maize and teosinte genotypes using grep and then transpose them using transpose.awk
		(head -n1 fang_et_al_genotypes.txt && grep -E -w "ZMMIL|ZMMLR|ZMMMR" fang_et_al_genotypes.txt)| \
		awk -f transpose.awk | tail -n+4 | \
		sort -k1,1 > transposed_maize_genotypes_sorted.txt
		
		(head -n1 fang_et_al_genotypes.txt && grep -E -w "ZMPBA|ZMPIL|ZMPJA" fang_et_al_genotypes.txt) | \
		awk -f transpose.awk | tail -n+4 | \
		sort -k1,1 > transposed_teosinte_genotypes_sorted.txt
		
		#get columns needed from snp position file and sort it to prepare for joining
		cut -f1,3,4 snp_position.txt | \
		tee snp_position_short.txt | \
		tail -n+2 | sort -k1,1 > snp_position_short_sorted.txt
		
		#join the datasets by SNP_ID column, sort them based on ascending (asc) or descending (dsc) order and replace missing values in dsc files with "-"
		(head -n1 snp_position_short.txt && join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_teosinte_genotypes_sorted.txt) | \
		awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' | \
		tee total_teosinte_genotypes_asc.txt | sed 's/?/-/g' | \
		awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_teosinte_genotypes_dash_dsc.txt
		
		(head -n1 snp_position_short.txt && join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_maize_genotypes_sorted.txt) | \
		awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' | \
		tee total_maize_genotypes_asc.txt | sed 's/?/-/g' | \
		awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_maize_genotypes_dash_dsc.txt
		
		#check results using wc
		wc total_*.txt snp_position_short*.txt > join_check.txt
		
		#check one set of results using awk to count columns
		awk -F "\t" 'NR>1{print NF; exit}' total_maize_genotypes_dash_dsc.txt > join_check_cols.txt
		awk -F "\t" 'NR>1{print NF; exit}' transposed_maize_genotypes_sorted.txt >> join_check_cols.txt
		
		#make directories to store output files
		mkdir -p snps_by_chr/{maize_asc,maize_dsc,teosinte_asc,teosinte_dsc}/
		
		#copy files with all chromosomes that need to be split
		mv total_maize_genotypes_asc.txt total_maize_genotypes_dash_dsc.txt total_teosinte_genotypes_asc.txt total_teosinte_genotypes_dash_dsc.txt snps_by_chr/
		
		#use awk to separate out based on chromosome and store the results within snps_by_chr in appropriate directories
		cd snps_by_chr/
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_asc.txt > maize_asc/snps_chr$chr.txt; done
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_dash_dsc.txt > maize_dsc/snps_chr$chr.txt; done
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_asc.txt > teosinte_asc/snps_chr$chr.txt; done
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_dash_dsc.txt > teosinte_dsc/snps_chr$chr.txt; done


* `snp_script.sh`: 

		#!/bin/bash
		set -e
		set -u
		set -o pipefail
		
		#script to process snp files for unix assignment
		#keep heads when sorting out maize and teosinte genotypes
		head -n1 fang_et_al_genotypes.txt > maize_genotypes.txt
		head -n1 fang_et_al_genotypes.txt > teosinte_genotypes.txt
		#use grep to separate the maize and teosinte snps
		grep -E -w "ZMMIL|ZMMLR|ZMMMR" fang_et_al_genotypes.txt >> maize_genotypes.txt #for maize genotypes
		grep -E -w "ZMPBA|ZMPIL|ZMPJA" fang_et_al_genotypes.txt >> teosinte_genotypes.txt # for teosinte genotypes
		#transpose the genotype files
		awk -f transpose.awk maize_genotypes.txt > transposed_maize_genotypes.txt
		awk -f transpose.awk teosinte_genotypes.txt > transposed_teosinte_genotypes.txt
		#get columns needed from snp position file
		cut -f1,3,4 snp_position.txt > snp_position_short.txt
		
		# get rid of header lines
		tail -n+4 transposed_maize_genotypes.txt | sort -k1,1 > transposed_maize_genotypes_sorted.txt 
		tail -n+4 transposed_teosinte_genotypes.txt | sort -k1,1 > transposed_teosinte_genotypes_sorted.txt 
		tail -n+2 snp_position_short.txt | sort -k1,1 > snp_position_short_sorted.txt
		
		#join the datasets by SNP_ID column
		join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_teosinte_genotypes_sorted.txt > joined_teosinte_genotypes.txt
		join -t $'\t' -1 1 -2 1 snp_position_short_sorted.txt transposed_maize_genotypes_sorted.txt > joined_maize_genotypes.txt
		
		#check join using wc
		wc joined_* snp_position_short_sorted.txt > join_check.txt
		
		#add SNP_ID Chromosome and Position headers back
		head -n1 snp_position_short.txt > total_teosinte_genotypes.txt
		head -n1 snp_position_short.txt > total_maize_genotypes.txt
		cat joined_teosinte_genotypes.txt >> total_teosinte_genotypes.txt
		cat joined_maize_genotypes.txt >> total_maize_genotypes.txt
		
		#sort based on ascending order
		cat total_teosinte_genotypes.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' > total_teosinte_genotypes_asc.txt
		cat total_maize_genotypes.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3n"}' > total_maize_genotypes_asc.txt
		
		
		#make dsc set with ? replaces with - 
		sed 's/?/-/g' total_maize_genotypes.txt > total_maize_genotypes_dash.txt
		sed 's/?/-/g' total_teosinte_genotypes.txt > total_teosinte_genotypes_dash.txt
		
		#sort based on descending order
		cat total_teosinte_genotypes_dash.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_teosinte_genotypes_dash_dsc.txt
		cat total_maize_genotypes_dash.txt | awk 'NR<2{print $0;next}{print $0| "sort -k2,2n -k3,3nr"}' > total_maize_genotypes_dash_dsc.txt
		#make directories for the final output
		mkdir -p snps_by_chr/{maize_asc,maize_dsc,teosinte_asc,teosinte_dsc}/
		
		#copy files with all chromosomes together that need to be split
		cp total_maize_genotypes_asc.txt total_maize_genotypes_dash_dsc.txt total_teosinte_genotypes_asc.txt total_teosinte_genotypes_dash_dsc.txt snps_by_chr/
		
		#use awk to separate out based on chromosome
		cd snps_by_chr/
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_asc.txt > maize_asc/snps_chr$chr.txt; done
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_maize_genotypes_dash_dsc.txt > maize_dsc/snps_chr$chr.txt; done
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_asc.txt > teosinte_asc/snps_chr$chr.txt; done
		for chr in {1..10}; do awk -v chr=$chr 'NR<2{print $0;next} $2 == chr' < total_teosinte_genotypes_dash_dsc.txt > teosinte_dsc/snps_chr$chr.txt; done
		
#R Assignment

* located in **`R_homework/`**

##Assignment R Notebook
* **`R_homework/R_Assignment_TN.Rmd`**: contains R notebook with assignment. 
	* The code is designed to be portable and can be run using the "knit" function so long as the `R_Assignment_TN.Rmd` file is inside the `R_homework/` directory. 

* Rendered Versions of the assignment
	* `R_homework/R_Assignment_TN.PDF`: A PDF of the of the notebook 
	* `R_homework/R_Assignment_TN.html`: HTML version of notebook

##Output files from Data Processing
* located in **`R_homework/data/`**
	* `R_homework/data/maize/asc/` contains the files for maize seperated by chromosome in ascending order of position with missing data encoded as "?"
	* `R_homework/data/maize/dsc/` contains the files for maize seperated by chromosome in descending order with missing data encoded as "-"
	* `data/teosinte/asc/` contains the files for teosinte seperated by chromosome in ascending order of position with missing data encoded as "?"
	* `data/teosinte/dsc/` contains the files for teosinte seperated by chromosome in descending order with missing data encoded as "-"

# Python Assignment

Python assignment is located in `python_assignment/`. More details can be found in the `README.md` within this directory. 


